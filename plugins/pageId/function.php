<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/13/19
 * Time: 9:47 PM
 */

function pageIdInit()
{
    registerPostMeta("pageId", 'Page Id', "pageIdMeta", ["page"], 'side');
}


function pageIdMeta($post, $plugin)
{
    $inputs = [
        "inputs" => [
            "page_id" => [
                "type" => "input",
                "label" => "Page Id"
            ]
        ]
    ];

    createInputFields($inputs, $post->ID, $plugin["args"]["pluginId"], $plugin["args"]["nonce"]);
}