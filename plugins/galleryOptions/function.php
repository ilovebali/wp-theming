<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/7/19
 * Time: 5:51 AM
 */


function galleryOptionsInit()
{
    registerPostMeta("galleryOptions", 'Home featured', "galleryOptionsMeta", [], "side");
}

function galleryOptionsMeta($post, $plugin)
{
    $inputs = [
        "inputs" => [
            "featured_home" => [
                "type" => "checkbox",
                "label" => "Home Featured?",
                "model" => "home_featured",
            ]
        ]
    ];

    createInputFields($inputs, $post->ID, $plugin["args"]["pluginId"], $plugin["args"]["nonce"]);
}