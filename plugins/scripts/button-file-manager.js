var buttonFileManager = {

    _buttons: [],
    _wpFileManager: null,
    _selectedFiles: [],
    _deleteButtons: [],

    init: function () {
        var fileManagerButton = document.querySelectorAll('.file-manager-button');
        this._deleteButtons = document.querySelectorAll('.button-delete-image-background');
        this._deleteImageBackground();

        if (!fileManagerButton) return;
        this._buttons = fileManagerButton;
        this._setButton();
    },


    _setButton: function () {

        var base = this;

        this._buttons.forEach(function (button) {
            var callback = button.getAttribute('data-fn');

            button.addEventListener('click', function () {
                base._openFileManager(callback)
            })

        })


    },

    _openFileManager: function (cb) {
        var fileFrame = null;
        var base = this;

        fileFrame = wp.media.frames.file_frame = wp.media({
            multiple: true
        });

        fileFrame.open();

        fileFrame.on('select', function () {
            var selectedData = fileFrame.state().get('selection').toJSON();
            for (var i = 0; i < selectedData.length; i++) {
                var file = selectedData[i];
                if (!file.sizes) return;
                var selectedFile = file.sizes;
                selectedFile.id = file.id;
                base._selectedFiles.push(selectedFile);
            }

            if (cb) window[cb](base._selectedFiles)
        });

        this._wpFileManager = fileFrame
    },

    _deleteImageBackground: function () {
        if (this._deleteButtons) {
            for (var i = 0; i < this._deleteButtons.length; i++) {
                var button = this._deleteButtons[i];

                button.index = i;

                button.addEventListener('click', function () {
                    var container = document.getElementById('identical-data-' + this.index);
                    container.parentNode.removeChild(container);
                })
            }
        }
    }


};


document.addEventListener("DOMContentLoaded", function () {
    buttonFileManager.init();
});