window.onSelectedFile = function (data) {
    Gallery.init(data);
};


var Gallery = {

    _lastIndex: 0,
    _data: null,
    _currentChildren: 0,
    _galleryContainer: null,

    init: function (data) {

        this._data = data;
        this._galleryContainer = document.getElementById('gallery-input-container');
        this._currentChildren = this._galleryContainer.children.length;
        this._lastIndex = this._currentChildren;
        this._create();
    },

    makeSortable: function () {
        var sortableDiv = jQuery("#gallery-input-container");
        sortableDiv.sortable();
        sortableDiv.disableSelection();
    },

    _create: function () {

        for (var i = 0; i < this._data.length; i++) {

            var data = this._data[i];
            var newItem = document.createElement('div');
            newItem.classList.add('gallery-input-item');
            newItem.innerHTML = this._template(this._lastIndex + i, data);
            this._galleryContainer.appendChild(newItem);
        }
    },

    _template: function (index, value) {

        return '<div class="identical-data-container">' +
            '<div class="input-container type-image-background" id="gallery-imageBackground-2" style="background-image: url(' + value.thumbnail.url + ')">\n' +
            '<div class="button-action" id="delete-button-2">Delete</div>' +
            '<input type="hidden" value="' + this._checkingImagePath(value, 'thumbnail') + '" name="gallery[' + index + '][thumb]">' +
            '<input type="hidden" value="' + this._checkingImagePath(value, 'large') + '" name="gallery[' + index + '][large]">' +
            '<input type="hidden" value="' + this._checkingImagePath(value, 'medium') + '" name="gallery[' + index + '][medium]">' +
            '<input type="hidden" value="' + this._checkingImagePath(value, 'full') + '" name="gallery[' + index + '][image]">' +
            '<input type="hidden" value="' + value.full.orientation + '" name="gallery[' + index + '][orientation]">' +
            '<input type="hidden" value="' + value.id + '" name="gallery[' + index + '][id]"></div></div>';

    },

    _checkingImagePath: function (object, model) {

        var image = null;

        if (object[model]) {
            image = object[model].url
        } else {
            image = ""
        }

        return image;

    }

};


document.addEventListener("DOMContentLoaded", function () {
    Gallery.makeSortable();
});