<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/30/18
 * Time: 1:52 PM
 */


function galleryInit()
{
    registerPostMeta("gallery", 'Image Gallery', "galleryMeta", ["page"]);
}


function galleryMeta($post, $plugin)
{
    $inputs = [
        "inputs" => [
            "gallery" => [
                "type" => "image_gallery",
                "gallery" => true
            ]
        ]
    ];

    createInputFields($inputs, $post->ID, $plugin["args"]["pluginId"], $plugin["args"]["nonce"]);
}