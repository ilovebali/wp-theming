<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/28/19
 * Time: 9:15 PM
 */
function imageFeaturedSecondaryInit()
{
    registerPostMeta("imageFeaturedSecondary", 'Main Banner', "imageFeaturedSecondaryMeta", [], "side");
}

function imageFeaturedSecondaryMeta($post, $plugin)
{
    $inputs = [
        "inputs" => [
            "imageFeaturedSecondary" => [
                "type" => "image_background",
                "model" => "medium",
            ]
        ]
    ];

    createInputFields($inputs, $post->ID, $plugin["args"]["pluginId"], $plugin["args"]["nonce"]);
}