<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/30/18
 * Time: 1:51 PM
 */

add_action('admin_enqueue_scripts', function () {
    $filePath = get_template_directory_uri() . '/admin.css?id=' . time();
    $buttonFileManager = get_template_directory_uri() . '/dist/script/admin-min.js?id=' . time();
    wp_register_style("plugin-hack", $filePath, false, '1.0.0');
    wp_enqueue_style("plugin-hack");
    wp_enqueue_script('button-file-manager', $buttonFileManager);
});