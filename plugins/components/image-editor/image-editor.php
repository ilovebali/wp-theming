<div id="<?php echo $detailInput['id'] ?>">
    <div class="image-container">
        <image-featured :featured="getImagePath" @remove="removeImage(index)"
                        @change="changeImage()"></image-featured>
        <template v-for="(file, key) of getModel">
            <input :key="key"
                   type="hidden"
                   :name="renderInputModel(details.options.inputId, key)" v-model="getModel[key]">
        </template>
    </div>
</div>

<script>
    window.DashaComponent(<?php echo createComponentDetail($detailInput) ?>, {
        data: {
            image: ''
        },

        computed: {
            getImagePath() {
                if (this.model && this.model.thumbnail) this.image = this.model.thumbnail
                return this.image
            }
        },

        methods: {
            changeImage() {
                const vm = this
                if (!vm.model) vm.model = {}
                this.openFileManager(function (selectedFiles) {
                    if (selectedFiles[0].sizes) {
                        const sizes = selectedFiles[0].sizes
                        for (const key in sizes) {
                            if (sizes.hasOwnProperty(key)) {
                                vm.model[key] = sizes[key].url
                            }
                        }
                    }
                    vm.image = selectedFiles[0].url
                }, false)
            },

            removeImage() {
                this.image = ''
                this.model = {}
            }
        }
    })
</script>