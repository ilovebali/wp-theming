<div id="<?php echo $detailInput['id'] ?>" class="panda__gallery-editor">
    <div class="panda__button-add__container">
        <div class="panda__button panda__button-add" @click="addMoreImage">Add Image</div>
    </div>
    <div class="card-container">
        <template v-if="getModel">
            <template v-for="(card, index) of getModel">
                <div class="outer-container">
                    <div class="image-container" :key="generateId(index)">
                        <image-featured :featured="card"
                                        @remove="removeImage(index)"
                                        @change="changeImage(index)"></image-featured>
                        <template v-for="(file, key) of card">
                            <input :key="key"
                                   type="hidden"
                                   :name="renderInputModel(details.options.inputId, key, index)"
                                   v-model="card[key]">
                        </template>
                    </div>
                </div>
            </template>
        </template>
    </div>
</div>

<script>
    window.DashaComponent(<?php echo createComponentDetail($detailInput) ?>, {
        methods: {
            generateId(index) {
                return index + new Date().getTime()
            },
            getImagePath(image) {
                return `url(${image})`
            },

            renderImageAttributeValue(imageSize) {
                let imageData = {}
                for (const key in imageSize) {
                    if (imageSize.hasOwnProperty(key)) {
                        imageData[key] = imageSize[key].url
                    }
                }
                return imageData
            },

            changeImage(index) {
                const model = this.model
                const vm = this
                this.openFileManager(function (selectedFiles) {
                    if (!selectedFiles) return
                    vm.model = null
                    if (selectedFiles[0].sizes) {
                        const imageSize = selectedFiles[0].sizes
                        model[index] = vm.renderImageAttributeValue(imageSize)
                    }
                    setTimeout(() => {
                        vm.model = model
                    }, 100)
                }, false)
            },

            removeImage(index) {
                this.model.splice(index, 1)
            },

            addMoreImage() {
                const vm = this
                if (!vm.model) vm.model = []
                this.openFileManager(function (selectedFiles) {
                    if (selectedFiles) {
                        selectedFiles.forEach(function (item) {
                            vm.model.push(vm.renderImageAttributeValue(item.sizes))
                        })
                    }
                })
            }
        }
    })
</script>