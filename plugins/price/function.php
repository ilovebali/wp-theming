<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/31/18
 * Time: 2:58 AM
 */

function priceInit()
{
    registerPostMeta("price", 'Manage Room Price', "priceMeta", [], "side");
}

function priceMeta($post, $plugin)
{
    $inputs = [
        "inputs" => [
            "regular_price" => [
                "type" => "input",
                "label" => "Regular Price",
                "model" => "regular_price",
            ],
            "promo_price" => [
                "type" => "input",
                "label" => "Hi Season",
                "model" => "hi_season",
            ],

            "extra_bed" => [
                "type" => "input",
                "label" => "Extra Bed",
                "model" => "extra_bed",
            ]
        ]
    ];

    createInputFields($inputs, $post->ID, $plugin["args"]["pluginId"], $plugin["args"]["nonce"]);
}