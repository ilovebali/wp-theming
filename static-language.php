<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 11/4/19
 * Time: 9:55 PM
 */
$language = [
    "accommodation" => "[:en]Accommodation[:de]Unterkunft[:da]Indkvartering",
    "services" => "[:en]Service & facilities[:de]Service & Einrichtung[:da]Service & faciliteter",
    "room" => "[:en]Our rooms[:de]Unsere zimmer[:da]Vores værelser",
    "discover" => "[:en]Discover More[:de]Erfahre Mehr[:da]Opdag Mere",
    "more" => "[:en]Read More[:de]Weiterlesen[:da]Læs mere",
    "price_detail" => "[:en]Extra Bed Incl. Breakfast %s <br/> Surcharge for high season %s <br/>(1 July - 15 Sept & 15 Dec - 10 Jan) <br/><br/><a href='mailto:contact@sunsukoretreat.com'>Contact us for special rate</a>
[:de]Extrabett Ink.Frühstück %s <br/>Hochsaisonzuschlag %s <br/>(1 July - 15 Sept & 15 Dec - 10 Jan) <br/><br/><a href='mailto:contact@sunsukoretreat.com'>Kontaktieren Sie uns, um ein Spezialpreis  zu erhalten</a>
[:da]Ekstra Seng Inkl.Morgenmad %s <br/>Tillæg for højsæsonen %s <br/>(1 July - 15 Sept & 15 Dec - 10 Jan) <br/><br/><a href='mailto:contact@sunsukoretreat.com'>Kontakt os for specielle tilbud</a>"

];


function getLanguage($key)
{
    global $language;
    return apply_filters("the_content", $language[$key]);
}