<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/23/18
 * Time: 9:20 PM
 */


$postType = [
    "facilities" => [
        "title" => "Facilities",
        "plugins" => [
            "gallery",
            "pageId"
        ],
        'supports' => ['title', 'thumbnail', 'editor']
    ],

    "accomodation" => [
        "title" => "Accommodation",
        'supports' => ['title', 'thumbnail', 'editor'],
        "plugins" => [
            "gallery",
            "price",
            "pageId",
            "imageFeaturedSecondary"
        ],
    ],

    "galleries" => [
        "title" => "Gallery",
        "plugins" => [
            "gallery",
            "galleryOptions",
            "pageId"
        ],
        'supports' => ['title', 'thumbnail']
    ]
];
