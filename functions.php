<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/23/18
 * Time: 9:12 PM
 */

$registeredMetaPlugin = array();


@include("woocommerce/woo-function.php");

@include "static-language.php";
/**
 * Register vue.js
 */

add_action('admin_enqueue_scripts', function () {
    wp_enqueue_script('vue-script', 'https://cdn.jsdelivr.net/npm/vue/dist/vue.js');
});

/**
 * Remove guttenberg
 */
add_filter('use_block_editor_for_post', '__return_false');

define('COCOLAB', "cocolab_super_hero");
@include "cores/helpers/function.php";
@include "cores/helpers/register-post-meta.php";
@include "cores/helpers/set-input.php";
@include "model/post-type.php";
@include "plugins/plugins.php";
@include "modules/theme-config.php";
@include "cores/helpers/get-options.php";
@include "cores/helpers/get-post-data.php";
@include "cores/helpers/set-post-type.php";
@include "cores/helpers/get-theme-template.php";
@include "components/components.php";

$getPostData = new GetPostData();
$options = new GetOptions(["google_map_option"]);
$getOption = $options->getOptions();

new SetPostType();


function getPageData($showChildren = false)
{
    global $getPostData;
    return $getPostData->getDetail($showChildren);
}


function getPostTypeListing($postTypeId)
{
    global $getPostData;
    return $getPostData->getPostTypeListing($postTypeId);
}

/**
 * Global variables
 */
$accommodations = getPostTypeListing('accomodation');


function getPageSlider()
{
    global $getPostData;
    if ($getPostData->getPageSlider()) {
        $slider = $getPostData->getPageSlider();

        var_dump($slider);

        if ($slider) {
            foreach ($slider as $key => $value) {
                $image = $value["image"];
                echo '<div class="item"><div class="background-container" style="background-image:url(' . $image . ')"></div></div>';
            }

        } else echo '<div class="hero_image full-height bg-cover bg-top-pos" style="background-image:url(' . $slider . ')"></div>';

    }
}


function getChildDetailByPageId($detail, $pageId)
{
    if ($detail->children) :
        foreach ($detail->children as $child) :
            if ($child->postMeta['pageId'] && $child->postMeta['pageId'] === $pageId) :
                return $child;
            endif;
        endforeach;
    endif;
}