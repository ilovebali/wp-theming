<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/24/18
 * Time: 1:00 AM
 */

add_theme_support('post-thumbnails');


remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


add_action('init', function () {
    register_nav_menus(
        [
            'primary-nav' => 'Top Navigation',
            'mobile-primary-nav' => 'Mobile Navigation',
            'footer-nav' => 'Footer Navigation',
            'lang' => 'Languages Navigation',
            'accommodation' => 'Footer Accommodation',
        ]
    );
});