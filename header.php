<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8"/>
    <title><?php bloginfo('name'); ?> - <?php the_title(); ?></title>
    <?php wp_head(); ?>
    <link rel="shortcut icon" href="http://craftedinbali.com/imgs/icon.png" type="image/png"/>
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, minimal-ui">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link href="<?php bloginfo('stylesheet_url'); ?>?<?php echo time() ?>" type="text/css" rel="stylesheet"/>

</head>

<BODY>
<?php @include "components/main-navigation.php"; ?>
<div id="form-inquiry" class="inquiry-form">
    <div class="form-container">
        <div class="close-form ti-close" id="close-form"></div>
        <?php echo do_shortcode('[contact-form-7 id="59" title="Reservation"]'); ?>
    </div>
</div>

<div id="gallery-overlay" class="fix-gallery">
    <div class="inner-container">
        <div class="button close-button __close ti-close"></div>
        <div class="main-picture __main"></div>
        <div class="thumbnails-gallery __thumbs"></div>
    </div>
</div>

<?php $facilities = getPostTypeListing('facilities'); ?>

<div id="facility-detail" class="facility-detail">
    <div class="button-close ti-close"></div>
    <div class="holder">
        <div class="wrapper">
            <div class="inner-container">
                <?php if ($facilities) : foreach ($facilities as $key => $item) : ?>
                    <div id="facility__<?php echo $key ?>" class="content-container">
                        <div class="content">
                            <h2 class="__font-heading"><?php echo $item->post_title ?></h2>
                            <div class="article">
                                <?php echo apply_filters('the_content', $item->post_content) ?>
                            </div>
                        </div>
                        <div class="banner">
                            <?php navigationSlider("slider-$key") ?>
                            <div class="slider__container" data-navigation="slider-<?php echo $key ?>"
                                 data-parallax=".parallax-container">
                                <?php if ($item->postMeta['gallery']) : foreach ($item->postMeta['gallery'] as $banner) : ?>
                                    <div class="featured">
                                        <div class="parallax-container"
                                             style="background-image: url(<?php echo $banner['full'] ?>)"></div>
                                    </div>
                                <?php endforeach;endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; endif; ?>
            </div>
        </div>
    </div>
</div>

<div id="main-container-wrapper" class="<?php echo is_page('contact') ? 'contact-main-wrapper ' : '' ?>">
    <div class="main-container-inner-wrapper">

