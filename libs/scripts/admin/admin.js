import {GalleryEditor} from "./includes/plugins/galleryEditor";
import {Component} from "./includes/plugins/Component"

class ButtonFileManager {
    constructor() {
        new GalleryEditor()
    }
}

window.DashaComponent = (detail, mixin, components)=> {
    new Component(detail, mixin, components)
}

document.addEventListener("DOMContentLoaded", () => {
    new ButtonFileManager()
});