import uuidv4 from 'uuid/v4';

export class GalleryEditor {
    constructor() {
        this._selectedButton = null
        this._selectedFiles = null
        this._init()
    }

    _init() {
        this._deleteButtons = document.querySelectorAll('.button-delete-image-background');
        this._buttonFileManager()
    }


    _buttonFileManager() {
        this._buttonFileManager = document.querySelectorAll('.file-manager-button');
        const galleryItems = document.querySelectorAll('.gallery-input-item')

        if (!this._buttonFileManager) return
        this._buttonFileManager.forEach(button => {
            button.addEventListener('click', () => {
                this._selectedButton = button
                this._openFileManager()
            })
        })

        if (galleryItems) {
            galleryItems.forEach(item => {
                const buttonDelete = item.querySelector('.button-action')
                if(buttonDelete) {
                    buttonDelete.addEventListener('click', () => {
                        item.parentNode.removeChild(item)
                    })
                }
            })
        }
    }

    _openFileManager() {
        let fileFrame = null;

        fileFrame = wp.media.frames.file_frame = wp.media({
            multiple: true
        });

        fileFrame.open();

        fileFrame.on('select', () => {
            this._selectedFiles = fileFrame.state().get('selection').toJSON();
            this._createInputTemplate()
        })
    }


    _renderImageUrlInputs(input) {

        if (!input) return ''
        const sizes = input.file.sizes
        const keys = Object.keys(sizes)

        if (keys.indexOf(input.size) > -1) {
            return `<input type="hidden" value="${sizes[input.size].url}" name="${input.model}[${input.index}][${input.size}]">`
        } else {
            return ''
        }

    }


    _createInputTemplate() {
        const button = this._selectedButton
        const buttonId = button.getAttribute('data-model')
        const inputContainer = document.getElementById(`${buttonId}-input-container`)
        const files = this._selectedFiles


        if (files) {

            files.forEach((file) => {
                let thumbUrl = file.url
                let inputsMedia = ''
                const index = uuidv4()
                const sizes = Object.keys(file.sizes)

                /**
                 * Create container for background
                 * @type {HTMLDivElement}
                 */
                const div = document.createElement('div')
                div.classList.add('gallery-input-item', 'dasha__image-container');
                div.setAttribute('id', `gallery-container-${index}`)

                const container = document.createElement('div')
                container.classList.add('identical-data-container')

                /**
                 * Create delete button
                 * @type {HTMLDivElement}
                 */
                const deleteButton = document.createElement('div')
                deleteButton.classList.add('button-action', 'button-action', 'button-delete-image-background')
                deleteButton.setAttribute('id', index)
                deleteButton.innerText = 'Delete'

                if (sizes) {
                    /**
                     * Checking for image display
                     */
                    if (sizes.indexOf('thumbnail') > -1) {
                        thumbUrl = file.sizes.thumbnail.url
                    }

                    /**
                     * Create inputs
                     */
                    sizes.forEach(size => {
                        inputsMedia += this._renderImageUrlInputs({
                            file,
                            model: buttonId,
                            index,
                            size
                        })
                    })
                }

                container.innerHTML = `<div class="input-container type-image-background" id="gallery-imageBackground-2" style="background-image: url(${thumbUrl})">
                                            ${inputsMedia}
                                            <input type="hidden" value="${file.id}" name="${buttonId}[${index}][id]">
                                        </div>`

                /**
                 * Add delete button
                 */
                container.appendChild(deleteButton)

                /**
                 * Add container
                 */
                div.appendChild(container)

                /**
                 * Add new div into container
                 */
                inputContainer.appendChild(div)


                deleteButton.addEventListener('click', () => {
                    const id = deleteButton.getAttribute('id')
                    const inputContainer = document.getElementById(`gallery-container-${id}`)
                    inputContainer.parentNode.removeChild(inputContainer)
                })
            })

        }
    }
}