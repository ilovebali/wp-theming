import TextInput from "../vue/TextInput.vue"
import TextAreaInput from "../vue/TextAreaInput.vue"
import ImageFeatured from "../vue/ImageFeatured.vue"

export class Component {
    constructor(props, mixin) {
        // console.log('--props--', props)
        this._mixin = mixin
        /**
         * Post get from original WP query
         * @type {any}
         */
        const post = props.post
        const options = props.input_detail


        this._props = {
            componentId: props.id,
            post: post,
            options: options
        }
        this._registerComponent()
    }


    _registerComponent() {

        const self = this
        new Vue({
            el: `#${this._props.componentId}`,
            components: {
                TextInput,
                ImageFeatured,
                TextAreaInput
            },
            data: {
                details: this._props,
                model: null
            },
            mixins: [
                this._mixin
            ],

            computed: {
                getModel() {
                    return this.model
                }
            },

            methods: {
                openFileManager(cb, isMultipleSelection = true) {
                    self._openFileManager(cb, isMultipleSelection)
                },

                renderInputModel(model, key, index = -1) {
                    if (index > -1) {
                        if (key) return `${model}[${index}][${key}]`
                        return `${model}[${index}]`
                    } else {
                        if (key) return `${model}[${key}]`
                        return model
                    }
                },

                createModelValue() {
                    if (self._props && self._props.post.postMeta) {
                        const model = self._props.options.inputId
                        this.model = self._props.post.postMeta[model]
                    }
                }
            },

            beforeMount() {
                this.createModelValue()
            }
        })
    }

    _openFileManager(cb, multiple) {
        let fileFrame = null;

        fileFrame = wp.media.frames.file_frame = wp.media({
            multiple: multiple
        });

        fileFrame.open();

        fileFrame.on('select', () => {
            const json = fileFrame.state().get('selection').toJSON()
            if (cb && json) cb(fileFrame.state().get('selection').toJSON())
        })
    }
}