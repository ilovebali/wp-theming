export const GlobalEditorMixin = {
    props: {
        name: {
            type: String,
            default: ''
        },

        details: {
            type: Object,
            default: null
        },

        index: {
            type: Number,
            default: -1
        },

        customKey: {
            type: String,
            default: ''
        }
    },
    computed: {
        getDetail() {
            return this.details
        },

        getInputDetail() {
            return this.getDetail.options
        },

        createModelValue() {
            const post = this.getDetail.post
            const postMeta = post.postMeta
            if (this.getInputDetail) {
                const input = this.getInputDetail.input
                if (postMeta) {
                    const mainModel = postMeta[this.getInputDetail.pluginId]
                    if (input.model) {
                        return mainModel[input.model]
                    } else {
                        return mainModel
                    }
                }
            }

            return ''
        }
    },


    methods: {
        renderInputModel() {
            const {options: {input, pluginId}} = this.details
            if (this.index > -1) {
                if (this.customKey) return `${input.model}[${this.index}][${this.customKey}]`
                return `${input.model}[${this.index}]`
            } else {
                if (this.customKey) return `${input.model}[${this.customKey}]`
                if (input.model) return `${pluginId}[${input.model}]`
                return `${pluginId}`
            }
        }
    }
}