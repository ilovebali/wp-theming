import {Sliders} from "./includes/Sliders"
import {Cards} from "./includes/Cards"
import {MasonryLayout} from "./includes/Masonry"
import {Navigation} from "./includes/Navigation"
import {Smooth} from "./includes/Smooth"
import {GalleryOverlay} from "./includes/GalleryOverlay"
import {ContactGoogleMap} from "./includes/ContactGoogleMap"

class FrontEnd {
    constructor() {
        FrontEnd.init()
    }

    static init() {
        new Sliders()
        new Cards()
        new MasonryLayout()
        new Navigation()
        new Smooth()
        new GalleryOverlay()
        new ContactGoogleMap()
    }
}

window.addEventListener('load', () => {
    new FrontEnd()
})
