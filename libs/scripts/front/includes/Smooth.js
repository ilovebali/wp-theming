import Scrollbar from "smooth-scrollbar"

export class Smooth {
    constructor() {
        this._smooth = null
        this.create()
    }

    create() {
        const bannerParallax = document.querySelectorAll('.banner__parallax')
        const contentParallax = document.querySelectorAll('.content__parallax')
        const container = document.getElementById('main-container-wrapper')
        this._smooth = Scrollbar.init(container);

        if (bannerParallax) {
            bannerParallax.forEach(parallax => {
                parallax.style.height = '120%'
                this._createParallax(parallax)
            })
        }

        if (contentParallax) {
            contentParallax.forEach(parallax => {
                this._createParallax(parallax)
            })
        }


        this._changeNavigationColor()
    }

    _createParallax(parallax) {
        let speed = 0.1
        const dataSpeed = parallax.getAttribute('data-speed')
        const isRevers = parallax.getAttribute('data-reverse')
        const disabledMobile = parallax.getAttribute('data-disabled-mobile')


        if (window.innerWidth < 700 && disabledMobile) {
            return
        }

        if (dataSpeed) {
            speed = dataSpeed / 100
        }

        const detail = parallax.getBoundingClientRect()
        this._smooth.addListener(event => {
            const pos = event.offset
            if (this._smooth.isVisible(parallax)) {
                const posY = detail.y - pos.y
                let transY = pos.y * speed / 2

                if (detail.y > 30) {
                    transY = posY * speed / 2
                }

                if (isRevers && window.innerWidth > 600) {
                    transY = transY * -1
                }
                parallax.style.transform = `translate3d(0,${transY}px,0)`
            }
        })
    }


    _changeNavigationColor() {
        const nav = document.querySelectorAll('.change__color-after-screen')
        const logo = document.getElementById('main-navigation-wrapper')
        this._smooth.addListener(event => {
            const pos = event.offset
            logo.style.transform = `translate3d(0,-${pos.y / 3}px,0)`
            if (pos.y >= window.innerHeight) {
                if (nav) nav.forEach(item => {
                    item.classList.add('change__color')
                })

                logo.classList.add('change__color')
            } else {
                if (nav) nav.forEach(item => {
                    item.classList.remove('change__color')
                })
                logo.classList.remove('change__color')
            }
        })
    }
}