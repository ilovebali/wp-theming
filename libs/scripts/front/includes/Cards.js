import {Power4, TimelineMax, TweenMax} from "gsap"

export class Cards {
    constructor() {
        this.readMoreDetail()
        this.cardFacilities()
    }

    readMoreDetail() {
        const buttons = document.querySelectorAll('.__button-read-more')

        if (buttons) {
            buttons.forEach(button => {
                button.addEventListener('click', () => {
                    const buttonId = button.getAttribute('id')
                    const target = document.getElementById(`${buttonId.replace('button-', '')}`)
                    if (target) {
                        const type = target.getAttribute('data-card-type')
                        console.log('type', type)
                        if (type) this[type](target)
                    }
                })
            })
        }
    }


    cardFacilities() {
        const buttonFacilities = document.querySelectorAll('.button__facilities')
        const detail = document.getElementById('facility-detail')
        const closeButton = detail.querySelector('.button-close')
        let activeItem = null
        if (buttonFacilities) {
            buttonFacilities.forEach(button => {
                button.addEventListener('click', () => {
                    const selector = button.getAttribute('data-item')
                    const item = document.getElementById(selector)
                    if (item) item.classList.add('active')
                    if (detail) detail.classList.add('active')

                    TweenMax.fromTo(detail, .6, {
                        height: '0px'
                    }, {
                        height: '100vh',
                        ease: Power4.easeInOut
                    })
                    activeItem = item
                })
            })
        }

        if (closeButton) {
            closeButton.addEventListener('click', () => {
                TweenMax.fromTo(detail, .6, {
                    height: '100vh'
                }, {
                    height: '0px',
                    ease: Power4.easeInOut,
                    onComplete: () => {
                        detail.classList.remove('active')
                        if (activeItem) activeItem.classList.remove('active')
                    }
                })
            })
        }
    }

    cardWithOverlayDescription(target) {
        target.classList.add('active')
        const closeButton = target.querySelector('.__close-button')
        const container = target.children
        const tween = new TimelineMax({
            onReverseComplete: () => {
                target.classList.remove('active')
            }
        })
        setTimeout(() => {
            tween.fromTo(target, .8, {
                x: '100%'
            }, {
                x: '0%',
                ease: Power4.easeInOut
            }).staggerFromTo(container, 1.2, {
                opacity: 0,
                y: 50
            }, {
                opacity: 1,
                y: 0,
                ease: Power4.easeInOut
            }, .13)
        }, false)

        if (closeButton) {
            closeButton.addEventListener('click', () => {
                TweenMax.to(target, .9, {
                    x: '-100%',
                    ease: Power4.easeInOut
                })
            })
        }
    }
}