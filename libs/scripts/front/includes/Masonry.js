import Masonry from "masonry-layout"
import imagesLoaded from "imagesloaded"

export class MasonryLayout {
    constructor() {
        this.create()
    }

    create() {
        const masonryElem = document.querySelectorAll('.masonry__layout')

        if (masonryElem) {
            console.log('masonryElem', masonryElem)
            masonryElem.forEach(masonry => {
                const masonryLayout = new Masonry(masonry, {
                    // options
                    itemSelector: '.grid-item',
                    columnWidth: 200,
                    percentPosition: true
                });

                const imageLoad = new imagesLoaded(masonry)
                console.log('imageLoad', imageLoad)
                imageLoad.on('progress', () => {
                    masonryLayout.layout();
                });
            })
        }
    }
}