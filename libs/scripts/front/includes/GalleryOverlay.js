import {Sliders} from "./Sliders"
import {Power4, TimelineMax} from "gsap"

export class GalleryOverlay {
    constructor() {
        this._slider = null
        this.create()
        this._tweenGallery = null
        this._gallery = document.getElementById('gallery-overlay')
        this._galleryThumbs = this._gallery.querySelector('.__thumbs')
        this._galleryMain = this._gallery.querySelector('.__main')
    }

    create() {
        const galleryButton = document.querySelectorAll('.button__gallery-overlay')

        if (galleryButton) {
            galleryButton.forEach(button => {
                const getItems = button.getAttribute('data-gallery')
                if (getItems) {
                    button.addEventListener('click', () => {
                        this._tweenGallery = new TimelineMax()
                        this._openPopup(window[getItems])
                    })
                }
            })
        }
    }


    _openPopup(items) {
        const closeButton = this._gallery.querySelector('.__close')
        const thumbId = `thumbs-${new Date().getTime()}`
        if (this._gallery) {

            this._tweenGallery.fromTo(this._gallery, .7, {
                y: '100%',
            }, {
                y: '0%',
                ease: Power4.easeInOut,
                onComplete: () => {

                    const innerContainer = this._gallery.querySelector('.inner-container')

                    setTimeout(() => {
                        innerContainer.classList.add('active')
                    }, 120)

                    if (this._galleryThumbs) {
                        this._galleryThumbs.innerHTML = this._createSliderItems(items)
                        this._galleryThumbs.classList.add('slider__container-gallery')
                        this._galleryThumbs.setAttribute('id', thumbId)
                    }

                    if (this._galleryMain) {
                        this._galleryMain.innerHTML = this._createSliderItems(items, true, '__main-picture-slider')
                        this._galleryMain.classList.add('slider__container-gallery')
                        this._galleryMain.setAttribute('data-dot-page', thumbId)
                        this._galleryMain.setAttribute('data-parallax', '.__main-picture-slider')
                    }


                    if (closeButton) {
                        closeButton.addEventListener('click', () => {
                            innerContainer.classList.remove('active')
                            if (this._tweenGallery) this._tweenGallery.reverse()
                            this._destroyGallery()
                        })
                    }
                    this._createSlider(this._galleryThumbs, this._galleryMain)
                }
            })
        }
    }


    _createSliderItems(items = [], useFullImage, className = '') {
        let thumbs = ''
        if (items) {
            for (let i = 0; i < items.length; i++) {
                const item = items[i]

                let image = item.thumbnail
                if (!image && !useFullImage) {
                    image = item.medium
                }

                if (useFullImage) {
                    image = item.image  || item.full
                }

                thumbs += `<div class="item dot"><div class="bg-cover ${className}" style="background-image: url(${image})"></div></div>`
            }
        }
        return thumbs
    }


    _createSlider(thumbs, main) {
        if (thumbs && main) {
            setTimeout(() => {
                this._slider = new Sliders('.slider__container-gallery')
            }, 100)
        }
    }

    _destroyGallery() {
        if (this._slider) {
            this._slider.destroyAll()
        }
        if (this._gallery) this._gallery.classList.remove('active')
        if (this._galleryThumbs) {
            this._galleryThumbs.innerHTML = ''
            this._galleryThumbs.classList.remove('slider__container')
        }
        if (this._galleryMain) {
            this._galleryMain.innerHTML = ''
            this._galleryThumbs.classList.remove('slider__container')
        }
    }
}