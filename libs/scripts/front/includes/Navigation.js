import {Power3, Power4, TimelineMax, TweenMax} from "gsap"

export class Navigation {
    constructor() {
        this.init()
        this.menuIsActive = false
        this.tween = null
        this.canClose = false
    }

    init() {
        const burger = document.getElementById('burger-navigation')
        const mainContainer = document.getElementById('main-navigation-container')
        const accommodationItems = document.getElementById('accommodation-navigation')
        const accommodationFeatured = document.getElementById('accommodation-featured-image')

        if (burger) {
            burger.addEventListener('click', () => {

                if (this.menuIsActive) {
                    if (!this.canClose) return
                    burger.classList.remove('active')
                    mainContainer.classList.remove('active')
                    TweenMax.to(mainContainer, .8, {
                        opacity: 0,
                        onComplete: () => {
                            this.tween = null
                            this.menuIsActive = false
                            this.canClose = false
                        },
                        ease: Power4.easeInOut
                    })

                    TweenMax.staggerFromTo('#main-navigation-container a', .8, {
                        opacity: 1,
                        y: 0
                    }, {
                        opacity: 0,
                        y: -100,
                        ease: Power4.easeInOut
                    }, .02)
                } else {
                    burger.classList.add('active')
                    mainContainer.classList.add('active')
                    this.tween = new TimelineMax()
                    this.tween.set(mainContainer, {
                        opacity: 1,
                        x: '100%'
                    })

                    this.tween.fromTo(mainContainer, .4, {
                        x: '100%'
                    }, {
                        x: '0%',
                        ease: Power3.easeInOut
                    }).staggerFromTo('#main-navigation-container a', 1, {
                        opacity: 0,
                        y: 100
                    }, {
                        opacity: 1,
                        y: 0,
                        ease: Power3.easeInOut
                    }, .02)
                        .fromTo(accommodationFeatured, 1, {
                            x: '-100%'
                        }, {
                            x: '0%',
                            ease: Power4.easeInOut,
                            onComplete: () => {
                                this.menuIsActive = true
                                this.canClose = true
                            }
                        })
                }
            })


            if (accommodationItems) {
                const child = accommodationItems.children
                const accommodationFeatured = document.querySelectorAll('.accommodation-featured-image-item')

                for (let i = 0; i < child.length; i++) {
                    const room = child[i]
                    room.addEventListener('mouseover', () => {
                        for (let k = 0; k < accommodationFeatured.length; k++) {
                            accommodationFeatured[k].classList.remove('active')
                        }
                        accommodationFeatured[i].classList.add('active')
                    })

                    room.addEventListener('mouseout', () => {
                        accommodationFeatured[i].classList.remove('active')
                    })
                }
            }
        }
    }
}