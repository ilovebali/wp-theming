import {Power4, TimelineMax} from "gsap"

export class ContactGoogleMap {
    constructor() {
        this._map = null
        this._markerImage = 'http://sunsukoretreat.com/wp-content/themes/sunsuko-retreat/assets/images/marker.png'
        this._options = {
            zoom: 15,
            center: {
                lat: -33.8927977,
                lng: 151.2010156
            }
        }

        this.create()
        this._showContactForm()
    }

    create() {
        if (window.mapLatLang) this._options.center = window.mapLatLang
        const mapContainer = document.getElementById('map-location')

        if (mapContainer) {
            this._map = new google.maps.Map(mapContainer, this._options);
            this._marker()
        }
    }

    _marker() {
        const markerIcon = new google.maps.MarkerImage(this._markerImage)
        markerIcon.scaledSize = new google.maps.Size(34, 50)
        return new google.maps.Marker({
            map: this._map,
            icon: markerIcon,
            position: this._options.center
        });
    }


    _showContactForm() {
        const button = document.getElementById('show-form')
        const closeButton = document.getElementById('close-form')
        let tween = null
        if (button) {
            button.addEventListener('click', () => {
                tween = new TimelineMax()
                tween
                    .fromTo('#form-inquiry', 1, {
                        x: '-100%'
                    }, {
                        x: '0%',
                        ease: Power4.easeInOut
                    })
                    .fromTo('#form-inquiry .form-container', 1, {
                        x: '-100%'
                    }, {
                        x: '0%',
                        ease: Power4.easeInOut
                    })
            })
        }

        if (closeButton) {
            closeButton.addEventListener('click', () => {
                if (tween) {
                    tween.duration(1)
                    tween.reverse()
                }
            })
        }
    }
}