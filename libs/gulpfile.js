const VueLoaderPlugin = require('vue-loader/lib/plugin')

const {src, dest, watch, parallel} = require('gulp');
const minify = require('gulp-minify');
const sass = require('gulp-sass');
const webpack = require("webpack-stream")


const scriptFiles = 'scripts/**/*.js'

function compileJs() {
    return src(scriptFiles)
        .pipe(webpack({
            entry: {
                front: './scripts/front/front.js',
                admin: './scripts/admin/admin.js',
            },
            module: {
                rules: [
                    // ... other rules
                    {
                        test: /\.vue$/,
                        loader: 'vue-loader'
                    }
                ]
            },
            plugins: [
                // make sure to include the plugin!
                new VueLoaderPlugin()
            ],
            watch: true,
            output: {
                filename: '[name]-min.js'
            }
        }))
        .pipe(dest('../dist/script'))
}

//
// function frontJS() {
//     return src(frontScript)
//         .pipe(webpack({
//             watch: true,
//             output: {
//                 filename: 'front-min.js'
//             }
//         }))
//         .pipe(dest('../dist/script'))
// }

function compileSCSS() {
    return src('scss/**/*.scss')
        .pipe(sass())
        .pipe(minify())
        .pipe(dest('../'))
}


function watchTask() {
    watch(['scss/**/*.scss'], compileSCSS)
}

exports.default = parallel(compileJs, watchTask)