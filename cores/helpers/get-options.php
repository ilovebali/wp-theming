<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/23/18
 * Time: 9:54 PM
 */


if (class_exists("GetOptions")) return;

class GetOptions
{
    private $options;

    public function __construct($options)
    {
        $this->options = $options;
    }


    public function getOptions()
    {

        $options = [];

        if (count($this->options)) {

            foreach ($this->options as $key => $option) {

                $options[$option] = get_option($option);
            }
        }

        return $options;
    }
}