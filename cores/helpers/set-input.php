<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/30/18
 * Time: 2:54 PM
 * @param $args
 * @param $postId
 * @param string $nonce
 * @param null $pluginId
 */
function createInputFields($args, $postId, $pluginId, $nonce)
{
    wp_nonce_field(plugin_basename(__FILE__), $nonce);
    $metaData = get_post_meta($postId, $pluginId, $single = true);

    if ($args) {

        echo "<div class='div-plugin-wrapper dasha__container'>";

        if ($args["file_manager"]) {
            $buttonLabel = $args["file_manager"]["button_label"];
            $jsCbFn = '';
            $model = '';
            if ($args["file_manager"]["callback_on_selected"]) $jsCbFn = $args["file_manager"]["callback_on_selected"];

            if ($args["file_manager"]["model"]) $model = $args["file_manager"]["model"];

            echo "<div class='file-manager-button $pluginId-file-manager' data-fn='$jsCbFn' data-model='$model'>$buttonLabel</div>";
        }

        if ($args["identical_data"]) {

            echo "<div id='$pluginId-input-container' class='$pluginId-input-container'>";
            if ($metaData) {

                echo "<script> window.dasha__" . $pluginId . " = " . json_encode($metaData);
                echo " </script>";

                foreach ($metaData as $id => $data) {
                    echo "<div class='$pluginId-input-item' id='identical-data-$id'>";

                    if ($args['inputs']) {

                        foreach ($args['inputs'] as $inputId => $input) {
                            echo "<div class='identical-data-container'>";
                            $argsInput = [
                                "pluginId" => $pluginId,
                                "index" => $id,
                                "data" => $data,
                                "input" => $input,
                                "isIdentical" => true,
                                "inputId" => $inputId
                            ];

                            new CreateInputDom($argsInput);
//                    createDom($input['type'], $label, $input, $data, $id);
                            echo "</div>\n\n";
                        }
                    }
                    echo "</div>\n\n";

                }
            }

            echo "</div>";

        } else {
            foreach ($args['inputs'] as $id => $input) {
                $argsInput = [
                    "pluginId" => $pluginId,
                    "input" => $input,
                    "data" => $metaData,
                    "inputId" => $id
                ];
                new CreateInputDom($argsInput);
            }
        }

        echo '</div>';

    }

}


class CreateInputDom
{

    private $pluginId;
    private $input;
    private $attribute;
    private $inputIndex;
    private $inputValue;
    private $inputModel;

    public function __construct($arg)
    {
        $this->input = $arg;
        $this->generateAttribute($arg);
        $this->create();
    }

    private function generateAttribute($arg)
    {


        $this->pluginId = $arg['pluginId'];
        if ($arg['index'] > -1) $this->inputIndex = $arg['index'];

        $this->inputModel = $arg['input']['model'];

        /**
         * If model is define on the input option
         * then get return the data value from model
         * if not just passed the data it self
         */
        if ($arg['data'] && $this->inputModel && $arg['data'][$this->inputModel]) {
            $this->inputValue = $arg['data'][$this->inputModel];
        } else {
            $this->inputValue = $arg['data'];
        }

        if ($arg['inputId']) {
            $this->attribute['inputId'] = $arg['pluginId'] . '-' . $arg['inputId'];
            if ($arg['isIdentical']) $this->attribute['inputId'] = $this->attribute['inputId'] . '-' . $this->inputIndex;

        } else {
            $this->attribute['inputId'] = $arg['pluginId'] . '-' . time();
        }

    }


    private function create()
    {

        switch ($this->input['input']['type']) {

            case "input" :
//                $this->createInput();
                $this->renderDataAndTemplate('input-text/input-text.php');
                break;

            case "checkbox" :
                $this->createCheckBoxInput();
                break;

            case "textarea" :
//                $this->createTextArea();
                $this->renderDataAndTemplate('input-text/input-textarea.php');
                break;

            case "image_gallery" :
                $this->renderDataAndTemplate('image-gallery/image-gallery.php');
                break;

            case "image_background" :
                $this->renderDataAndTemplate('image-editor/image-editor.php');
                break;

            case "image" :
                $this->createImage();
                break;
        }

    }


    protected function renderDataAndTemplate($path)
    {
        $detailInput = getComponentDetail($this->input);
        $templatePath = get_template_directory() . '/plugins/components/' . $path;
        require($templatePath);
    }

    private function createInput()
    {

        $id = $this->attribute['inputId'];


        $input = $this->createInputDom();
        echo "<div class='input-container' id='$id'>
            <label>" . ($this->input['input']['label'] ? $this->input['input']['label'] : $this->pluginId) . "</label>
            $input
          </div>";

    }


    private function createCheckBoxInput()
    {

        $id = $this->attribute['inputId'];
        $input = $this->createInputDom('checkbox');
        echo "<div class='input-container' id='$id'>
            <label>" . ($this->input['input']['label'] ? $this->input['input']['label'] : $this->pluginId) . "</label>
            $input
          </div>";
    }

    private function createInputDom($type = "text", $customModel = null)
    {
        $detail = $this->renderInputDetail($customModel);

        if ($type === 'checkbox') {

            $selected = '';
            if ($detail['value']) {
                $selected = 'checked';
            }
            return $input = "<input type='$type' $selected name='$detail[model]'>";
        } else {
            return $input = "<input type='$type' value='$detail[value]' name='$detail[model]'>";
        }
    }

    function createTextArea()
    {

    }


    function createImageBackground()
    {
        $id = $this->attribute['inputId'];

        echo "<div class='input-container type-image-background' id='$id' style='background-image: url($this->inputValue)'>
            <div class='button-action button-delete-image-background' id='delete-button-$this->inputIndex'>Delete</div>";
        echo $this->createInputDom('hidden');
        echo $this->createInputDom('hidden', 'large');
        echo $this->createInputDom('hidden', 'medium');
        echo $this->createInputDom('hidden', 'image');
        echo $this->createInputDom('hidden', 'id');
        echo $this->createInputDom('hidden', 'orientation');
        echo "</div>";

    }


    function createImage()
    {
    }


    protected function renderInputDetail($customModel = null)
    {

        $model = $this->pluginId . "[" . $this->inputModel . "]";
        $value = $this->inputValue;

        if (!$this->inputModel) {
            $model = $this->pluginId;
            $value = $this->inputValue;
        }


        if (!$this->input['data']) return [];

        if ($customModel && !$this->input['isIdentical']) {
            $model = "$this->pluginId[$this->inputIndex][$customModel]";
        }

        if ($this->input['isIdentical']) {
            $model = "$this->pluginId[$this->inputIndex][" . $this->input['input']['model'] . "]";

            if ($customModel) $model = "$this->pluginId[$this->inputIndex][$customModel]";
        }


        if ($customModel) $value = $this->input['data'][$customModel];


        return ['value' => $value, 'model' => $model];
    }

}


function updatePostMeta($post_id)
{
    global $registeredMetaPlugin;
    $activeNonce = [];

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

    if ('page' === $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) return;
    } else {
        if (!current_user_can('edit_post', $post_id)) return;
    }


    foreach ($registeredMetaPlugin as $key => $plugin) {
        $nonce = $plugin["nonce"];
        if (wp_verify_nonce($_POST[$nonce], plugin_basename(__FILE__)) && isset($_POST[$nonce])) {
            array_push($activeNonce, $plugin['pluginId']);
        }
    }


    if (!count($activeNonce)) return;

    foreach ($activeNonce as $plugin) {
        $cleanerPost = [];
        $post = $_POST[$plugin];


        if (is_array($post)) {
            foreach ($post as $key => $value) {
                if (is_numeric($key)) {
                    array_push($cleanerPost, $value);
                } else {
                    $cleanerPost[$key] = $value;
                }

            }
        } else {
            $cleanerPost = $post;
        }

        add_post_meta($post_id, $plugin, $cleanerPost, true) or
        update_post_meta($post_id, $plugin, $cleanerPost);
    }

}