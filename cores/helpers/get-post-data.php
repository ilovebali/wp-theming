<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/23/18
 * Time: 9:54 PM
 */


if (class_exists("GetPostData")) return;

class GetPostData
{
    private $postId;
    private $postDetail;
    private $options;

    public function __construct()
    {
    }

    private function getGallery($postId)
    {
        $gallery = get_post_meta($postId, "gallery", true);
        return $gallery;
    }


    private function getPostMeta($post)
    {
        $postMeta = get_post_meta($post->ID);
        $metaData = [];

        if ($postMeta) {
            foreach ($postMeta as $key => $meta) {
                $postMetaData = get_post_meta($post->ID, $key, true);
                if ($postMetaData) $metaData[$key] = $postMetaData;
            }
        }

        $post->postMeta = $metaData;
    }

    private function getFeaturedImage($postId)
    {
        if (has_post_thumbnail($postId)) {
            return get_the_post_thumbnail_url($postId);
        }

        return null;
    }

    public function getDetail($includeChild = false)
    {
        global $post;
        $this->postId = $post->ID;
        $this->postDetail = $post;
        $this->getPostMeta($post);


        if ($includeChild) {
            $args = array(
                'post_parent' => $post->ID,
                'post_type' => $post->post_type,
                'numberposts' => -1,
                'post_status' => 'published'
            );
            $children = get_children($args);

            if ($children) {
                $post->children = $children;
                foreach ($children as $child) {
                    $this->getPostMeta($child);
                    $this->gallery($child);
                }
            }

        }

        $this->gallery($post);

        return $post;
    }


    function gallery($post)
    {
        if ($this->getGallery($post->ID)) $post->gallery = $this->getGallery($post->ID);
        if ($this->getFeaturedImage($post->ID)) $post->featured_image = $this->getFeaturedImage($post->ID);
    }


    public function getPageSlider()
    {
        $gallery = null;

        if (!$this->postDetail) $this->getDetail();

        if (count($this->postDetail->gallery)) $gallery = $this->postDetail->gallery;
        if ($this->getFeaturedImage($this->postId)) $gallery = $this->getFeaturedImage($this->postId);

        return $gallery;

    }

    public function getPostTypeListing($postTypeId, $postPerPage = 20)
    {
        $args = array('post_type' => $postTypeId, 'posts_per_page' => $postPerPage);

        if ($this->options) {
            $args = array_push($args, $this->options);
        }

        $postRaw = new WP_Query($args);

        if ($postRaw->posts) {

            foreach ($postRaw->posts as $key => $post) {
                if ($this->getGallery($post->ID)) $post->gallery = $this->getGallery($post->ID);
                if ($this->getFeaturedImage($post->ID)) $post->featured_image = $this->getFeaturedImage($post->ID);
                $post->slug = get_permalink($post->ID);

                if (!$post->ecerpt) $post->ecerpt = $this->shortenText($post->post_content, 100);

                $this->getPostMeta($post);
            }

            return $postRaw->posts;
        }

        return null;
    }

    protected function shortenText($string, $wordsReturned)
    {
        $retval = $string;
        $string = preg_replace('/(?<=\S,)(?=\S)/', ' ', $string);
        $string = str_replace("\n", " ", $string);
        $string = wp_strip_all_tags($string);
        $array = explode(" ", $string);
        if (count($array) <= $wordsReturned) {
            $retval = $string;
        } else {
            array_splice($array, $wordsReturned);
            $retval = implode(" ", $array) . " ...";
        }
        return $retval;
    }
}