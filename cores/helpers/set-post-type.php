<?php


if (class_exists('SetPostType')) return;

class SetPostType
{
    public $post_type_name;
    public $post_type_args;
    public $post_type_labels;

    private $postTypes = [];


    /* Class constructor */
    public function __construct()
    {
        global $postType;
        $this->postTypes = $postType;
        add_action('init', array(&$this, 'register_post_type'));
        $this->add_meta_box();
    }


    /* Method which registers the post type */
    public function register_post_type()
    {


        foreach ($this->postTypes as $key => $cpt) {

            $args = array(
                'public' => true,
                'label' => $cpt["title"],
                'supports' => $cpt['supports']
            );


            if ($cpt["register_to_rest"]) {
                $args['show_in_rest'] = true;
            }

            register_post_type($key, $args);
        }
    }


    /* Method to attach the taxonomy to the post type */
    public function add_taxonomy($name, $slug, $args = array(), $labels = array())
    {

        if (!empty($name)) {
            // We need to know the post type name, so the new taxonomy can be attached to it.
            $post_type_name = $this->post_type_name;

            // Taxonomy properties
            $taxonomy_labels = $labels;
            $taxonomy_args = $args;

            /* More code coming */
            if (!taxonomy_exists($slug)) {
                //Capitilize the words and make it plural
                $name = ucwords(str_replace('-', ' ', $name));
                $plural = $name;

                // Default labels, overwrite them with the given labels.
                $labels = array_merge(

                // Default
                    array(
                        'name' => _x($plural, 'taxonomy general name'),
                        'singular_name' => _x($name, 'taxonomy singular name'),
                        'search_items' => __('Search ' . $plural),
                        'all_items' => __('All ' . $plural),
                        'parent_item' => __('Parent ' . $name),
                        'parent_item_colon' => __('Parent ' . $name . ':'),
                        'edit_item' => __('Edit ' . $name),
                        'update_item' => __('Update ' . $name),
                        'add_new_item' => __('Add New ' . $name),
                        'new_item_name' => __('New ' . $name . ' Name'),
                        'menu_name' => __($name),
                    ),

                    // Given labels
                    $taxonomy_labels

                );

                // Default arguments, overwritten with the given arguments
                $args = array_merge(

                // Default
                    array(
                        'label' => $plural,
                        'labels' => $labels,
                        'hierarchical' => true,
                        'public' => true,
                        'show_ui' => true,
                        'show_in_nav_menus' => true,
                        '_builtin' => false,
                    ),

                    // Given
                    $taxonomy_args

                );


                // Add the taxonomy to the post type
                add_action('init',
                    function () use ($slug, $post_type_name, $args) {
                        register_taxonomy($slug, $post_type_name, $args);
                    }
                );

            } else {

                add_action('init',
                    function () use ($slug, $post_type_name) {
                        register_taxonomy_for_object_type($slug, $post_type_name);
                    }
                );

            }
        }

    }

    /* Attaches meta boxes to the post type */
    public function add_meta_box()
    {

        foreach ($this->postTypes as $key => $post) {
            if (!$post["plugins"]) return;
            foreach ($post["plugins"] as $plugins) {
                $templatePath = get_template_directory() . '/plugins/' . $plugins . '/function.php';
                load_template($templatePath);
                add_action('admin_init', $plugins . 'Init');
            }
        }
    }
}
