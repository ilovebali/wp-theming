<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/31/18
 * Time: 10:52 AM
 * @param $id
 * @param $label
 * @param $function
 * @param array $includeScreen
 * @param string $position
 */

/**
 * @param $id
 * @param $label
 * @param $function
 * @param array $includeScreen
 * @param string $position
 */
function registerPostMeta($id, $label, $function, $includeScreen = [], $position = "advanced")
{
    global $postType, $registeredMetaPlugin;

    $pluginName = $label;
    $pluginRegisteredId = time();
    $postScreen = $includeScreen;
    foreach ($postType as $postId => $value) {

        if ($value['plugins']) {
            foreach ($value['plugins'] as $plugin) {
                if ($plugin === $id) {
                    array_push($postScreen, $postId);
                }
            }

        }
    }


    foreach ($postScreen as $key => $post) {
        $nonce = COCOLAB . $key . $id;
        $arg = [
            "nonce" => $nonce,
            "pluginId" => $id
        ];

        array_push($registeredMetaPlugin, $arg);
        add_action('save_post', 'updatePostMeta');
        add_meta_box(COCOLAB . "-" . $id . $pluginRegisteredId, __($pluginName, COCOLAB), $function, $post, $position, 'default', $arg);
    }

}


function registerPostMetaStyleScript($files)
{

    new RegisterStyleScript($files);
}


class RegisterStyleScript
{

    private $files;
    private $pluginId;

    public function __construct($files)
    {
        $this->pluginId = $files["id"];
        $this->files = $files;
        add_action('admin_enqueue_scripts', array(&$this, 'addStyleScript'));
    }


    public function addStyleScript()
    {


        if (!$this->files) return;

        if (count($this->files["files"])) {


            if ($this->files["files"]['style'] && count($this->files["files"]['style'])) {
                foreach ($this->files["files"]['style'] as $style) {
                    $filePath = get_template_directory_uri() . '/plugins/' . $this->pluginId . '/styles/' . $style . ".css";
                    wp_register_style("$style", $filePath, false, '1.0.0');
                    wp_enqueue_style("$style");
                }
            }


            if ($this->files["files"]['script'] && count($this->files["files"]['script'])) {
                foreach ($this->files["files"]['script'] as $script) {
                    $filePath = get_template_directory_uri() . '/plugins/' . $this->pluginId . '/scripts/' . $script . ".js";
                    wp_enqueue_script($script . '-' . time(), $filePath);
                }
            }

        }
    }
}