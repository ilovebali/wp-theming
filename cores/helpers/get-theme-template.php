<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/23/18
 * Time: 9:45 PM
 */

if (function_exists('getTemplatePart')) return;

function getTemplatePart()
{
    global $wp_query;
    $postType = $wp_query->query_vars['post_type'];

    if ($wp_query->is_single) {
        get_template_part("components/$postType/single");
    } elseif ($wp_query->is_page && !$wp_query->is_front_page()) {
        get_template_part("components/page/page", $wp_query->query["pagename"]);
    } elseif ($wp_query->is_attachment) {
        get_template_part("components/attachment/single");
    } else {
        if ($wp_query->is_front_page()) {
            get_template_part("components/home/single");
        } elseif ($wp_query->query['pagename'] === 'blog') {
            get_template_part("themes/page/blog");
        } else {
            get_template_part("themes/$postType/post");
        }
    }


}

