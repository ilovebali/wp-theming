<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/28/19
 * Time: 9:33 PM
 * @param $detail
 * @return array
 */

function getComponentDetail($detail)
{
    $currentPost = getPageData();
    return [
        "id" => uniqid('component_'),
        "post" => $currentPost,
        "input_detail" => $detail
    ];
}

function createComponentDetail($data){
    return json_encode($data);
}