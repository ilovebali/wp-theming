import {Sliders} from "./includes/Sliders"
import {ProductAttribute} from "./includes/ProductAttribute"

class Front {
    constructor() {
        new Sliders('.bs__slider-container')
        new ProductAttribute()
    }
}

window.addEventListener('load', () => {
    new Front()
})
