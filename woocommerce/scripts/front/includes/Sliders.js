import Flickity from "flickity"
import {Power3, Power4, TimelineMax, TweenMax} from "gsap"

export class Sliders {
    constructor(sliderSelector) {
        this._sliderSelector = sliderSelector || '.slider__container'
        this._sliders = []
        this._sliderGallery()
        this._currentActiveSlide = 0
        this._isSlide = false
        return this
    }

    _sliderGallery() {

        if (typeof this._sliderSelector === 'string') {
            const sliders = document.querySelectorAll(this._sliderSelector)
            if (sliders) {
                sliders.forEach(slider => {
                    this._create(slider)
                })
            }
        } else {
            console.log('this._sliderSelector', this._sliderSelector)
            this._create(this._sliderSelector)
        }
    }


    _create(slider) {
        const splitSlider = slider.getAttribute('data-type')
        const dataOptions = slider.getAttribute('data-options')
        const navigation = slider.getAttribute('data-navigation')
        const parallax = slider.getAttribute('data-parallax')
        const dotPage = slider.getAttribute('data-dot-page')
        const disabledAtMinWidth = slider.getAttribute('data-disabled-at')

        let defaultOptions = {
            contain: true,
            prevNextButtons: false,
            pageDots: false,
            selectedAttraction: .019
        }

        if (dataOptions) {
            const customOptions = JSON.parse(dataOptions)
            defaultOptions = Object.assign(defaultOptions, customOptions)
        }

        if (disabledAtMinWidth && window.innerWidth < disabledAtMinWidth) return
        const flickity = new Flickity(slider, defaultOptions)

        /**
         * Push into array
         */
        this._sliders.push(flickity)


        /**
         * If custom prev next navigation provide
         * we will user this navigation
         */
        if (navigation) {
            const navigationPrevNext = document.getElementById(navigation)
            const prev = navigationPrevNext.querySelector('.__prev')
            const next = navigationPrevNext.querySelector('.__next')

            /**
             * Disabled the prev button if first index
             */
            if (flickity.selectedIndex === 0) {
                prev.classList.add('disabled')
            }

            if (prev) {
                prev.addEventListener('click', () => {
                    flickity.previous()
                })
            }

            if (next) {
                next.addEventListener('click', () => {
                    flickity.next()
                })
            }

            flickity.on('scroll', () => {

                prev.classList.remove('disabled')
                next.classList.remove('disabled')

                /**
                 * Disabled the prev button if first index
                 */
                if (flickity.selectedIndex === 0) {
                    prev.classList.add('disabled')
                }

                if (flickity.selectedIndex === (flickity.cells.length - 1)) {
                    next.classList.add('disabled')
                }
            })
        }

        if (splitSlider) {
            /**
             * Get slider destination
             *
             * @type {string[]}
             */
            const splitSliderDetail = splitSlider.split(':')
            if (splitSliderDetail) {
                const fakeSlider = document.getElementById(splitSliderDetail[1])
                const fakeSliderChild = fakeSlider.children
                /**
                 * Set fist slider to be active
                 */
                fakeSliderChild[0].classList.add('active')

                if (splitSliderDetail.length > 2) {
                    const navigation = document.getElementById(splitSliderDetail[2])
                    const children = navigation.children

                    /**
                     * Create page slider navigation base on children
                     */
                    if (children) {
                        for (let i = 0; i < children.length; i++) {
                            const page = children[i]
                            page.addEventListener('click', () => {
                                if (this._isSlide) return
                                flickity.select(i)
                                this._isSlide = true
                                this._fakeSlider(fakeSliderChild, flickity)
                                /**
                                 * Removing class Active
                                 */
                                for (let j = 0; j < children.length; j++) {
                                    const item = children[j]
                                    item.classList.remove('disabled')
                                    item.classList.add('active')
                                }
                                page.classList.remove('active')
                                page.classList.add('disabled')
                            })
                        }
                    }
                }
            }
        }

        if (parallax) {
            const elem = slider.querySelectorAll(parallax)
            this._parallaxSlider(flickity, elem)
        }


        if (dotPage) {
            this._customDotPage(flickity, dotPage)
        }
    }

    _parallaxSlider(slider, background) {
        const docStyle = document.documentElement.style;
        const transformProp = typeof docStyle.transform === 'string' ? 'transform' : 'WebkitTransform';

        slider.on('scroll', () => {
            slider.slides.forEach((slide, i) => {
                const img = background[i];
                const x = (slide.target + slider.x) * -1 / 1.24;
                if (img) img.style[transformProp] = 'translateX(' + x + 'px)';
            });
        });

    }

    _fakeSlider(fake, flickity) {
        if (fake) {
            if (flickity) {
                const tween = new TimelineMax()
                const historySlider = fake[this._currentActiveSlide]
                const activeElement = fake[flickity.selectedIndex]
                const activeElementChildren = activeElement.children

                historySlider.classList.remove('active')
                TweenMax.to(historySlider, .9, {
                    opacity: 0,
                    y: 40,
                    ease: Power4.easeInOut
                })

                tween.to(activeElement, .1, {
                    opacity: 1,
                    y: 0,
                    ease: Power4.easeInOut
                }).staggerFromTo(activeElementChildren, 1.2, {
                    opacity: 0,
                    y: 90,
                    scaleY: 1.3
                }, {
                    opacity: 1,
                    y: 0,
                    scaleY: 1,
                    ease: Power4.easeInOut,
                    onComplete: () => {
                        this._currentActiveSlide = flickity.selectedIndex
                        this._isSlide = false
                        activeElement.classList.add('active')
                    }
                }, .12)
            }
        }
    }

    _customDotPage(slider, dotPage) {
        const el = document.getElementById(dotPage)

        if (el) {
            const child = el.querySelectorAll('.dot')
            if (child) {
                for (let i = 0; i < child.length; i++) {
                    const dot = child[i]
                    child[0].classList.add('active')
                    dot.addEventListener('click', () => {
                        for (let k = 0; k < child.length; k++) {
                            child[k].classList.remove('active')
                        }
                        dot.classList.add('active')
                        slider.select(i)
                    })
                }

                slider.on('scroll', () => {
                    const activeIndex = slider.selectedIndex
                    for (let k = 0; k < child.length; k++) {
                        child[k].classList.remove('active')
                    }
                    if (child[activeIndex]) child[activeIndex].classList.add('active')
                })
            }
        }
    }

    destroyAll() {
        if (this._sliders) {
            this._sliders.forEach(slider => {
                slider.destroy()
            })
        }
    }
}