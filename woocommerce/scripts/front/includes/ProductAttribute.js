import {Helper} from "./Helper"
import Flickity from "flickity"
import {Sliders} from "./Sliders"

export class ProductAttribute {
    constructor() {
        this.optionsActive = []
        this.form()
        this.customSelect()
    }

    customSelect() {
        const formSelector = Helper.selector("form.variations_form")
        if (formSelector && formSelector.length > 0) {
            const form = formSelector[0]
            if (form) {

                /**
                 * Listen jQuery custom event
                 * because jQuery provides an event layer over vanilla JS
                 * https://stackoverflow.com/questions/40915156/listen-for-jquery-event-with-vanilla-js
                 */
                jQuery('form.variations_form').on('wc_additional_variation_images_frontend_ajax_response_callback', () => {

                    if (Helper.selector(".woocommerce-product-gallery__wrapper")) {
                        const slider = Flickity.data(".woocommerce-product-gallery__wrapper")
                        if (slider) {
                            slider.destroy()
                            setTimeout(() => {
                                new Sliders(".woocommerce-product-gallery__wrapper")
                            }, 100)
                        } else {
                            new Sliders(".woocommerce-product-gallery__wrapper")
                        }
                    }
                })

                const customOptions = form.querySelectorAll(".__attribute-select")
                const optionsContainer = form.querySelectorAll(".custom__select-options")
                if (customOptions) {
                    for (let i = 0; i < customOptions.length; i++) {
                        const buttonSelectOption = optionsContainer[i]
                        const button = customOptions[i]

                        this.optionsActive[i] = {}

                        button.addEventListener('click', () => {
                            const selectId = button.getAttribute('data-form')
                            const select = document.getElementById(selectId)
                            const options = select.getElementsByTagName('option')

                            if (this.optionsActive[i].active) {
                                buttonSelectOption.innerHTML = ''
                                this.optionsActive[i].active = false
                                return
                            }

                            this.optionsActive.forEach(item => {

                                console.log('item', item)

                                if (item) {
                                    if (item.dom) item.dom.innerHTML = ''
                                    if (item.active) item.active = false
                                }
                            })

                            this.form()

                            if (options) {
                                for (let j = 0; j < options.length; j++) {

                                    /**
                                     * Get option information
                                     * @type {HTMLOptionElement}
                                     */
                                    const option = options[j]
                                    const value = option.value
                                    const innerText = option.innerText
                                    const thumbImage = option.getAttribute('data-background')

                                    if (value) {

                                        /**
                                         * Create item option
                                         * @type {HTMLDivElement}
                                         */
                                        const item = document.createElement('div')
                                        item.classList.add('__option')
                                        item.setAttribute('data-value', value)
                                        if (thumbImage) {
                                            item.innerHTML = `<div class="thumb" style="background-image: url(${thumbImage})"></div><div class="label">${innerText}</div>`
                                        } else {
                                            item.innerHTML = `<div class="label">${innerText}</div>`
                                        }
                                        buttonSelectOption.appendChild(item)


                                        /**
                                         * Item event listener
                                         */
                                        item.addEventListener('click', () => {
                                            buttonSelectOption.innerHTML = ''
                                            this.optionsActive[i].active = false
                                            select.value = value
                                            button.innerText = item.innerText

                                            /**
                                             * Trigger event on change
                                             * this will help WooCommerce still can listen changing on select option
                                             */
                                            if ("createEvent" in document) {
                                                const evt = document.createEvent("HTMLEvents");
                                                evt.initEvent("change", true, true);
                                                select.dispatchEvent(evt);
                                            }
                                            else {
                                                select.fireEvent("onchange");
                                            }
                                        })
                                    }
                                }
                            }

                            this.optionsActive[i] = {
                                active: true,
                                dom: buttonSelectOption
                            }
                        })
                    }
                }
            }
        }
    }

    form() {
        const form = Helper.selector(".cart")
        if (form && form.length > 0) {
            const cart = form[0]
            const formAttribute = cart.getAttribute('data-product_variations')
            if (formAttribute) {
                const jsonData = JSON.parse(formAttribute)
                for (let i = 0; i < jsonData.length; i++) {
                    const data = jsonData[i]
                    const attribute = data.attributes

                    if (attribute && attribute.image) {
                        for (const key in attribute.image) {

                            if (attribute.image.hasOwnProperty(key)) {
                                const image = attribute.image[key]
                                const select = document.getElementById(key)
                                if (select) {
                                    const options = select.getElementsByTagName('option')
                                    if (options) {
                                        for (let t = 0; t < options.length; t++) {
                                            const option = options[t]
                                            const getValue = option.value
                                            if (getValue === image.attr_id) {
                                                option.setAttribute('data-background', image.url)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}