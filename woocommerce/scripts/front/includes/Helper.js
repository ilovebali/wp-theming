export class Helper {
    static selector(selector) {
        if (selector) {
            if (selector.includes('.')) {
                return document.querySelectorAll(selector)
            }
            return document.getElementById(selector)
        }

        return null
    }
}