<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 11/2/19
 * Time: 8:51 PM
 */

defined('ABSPATH') || exit;
@include "hooks/hooks.php";


/**
 * Remove unused hook
 */
$removeHook = [
    [
        "woocommerce_before_shop_loop" => ["woocommerce_catalog_ordering", 30],
    ],
    [
        "woocommerce_before_shop_loop" => ["woocommerce_result_count", 20],
    ],

    [
        "woocommerce_after_single_product_summary" => ["woocommerce_output_related_products", 20],
    ],

    [
        "woocommerce_single_product_summary" => ["woocommerce_template_single_title", 5],
    ],

    [
        "woocommerce_single_product_summary" => ["woocommerce_template_single_excerpt", 20],
    ],

    [
        "woocommerce_single_product_summary" => ["woocommerce_template_single_meta", 40],
    ],
    [
        "woocommerce_before_single_product_summary" => ["woocommerce_show_product_sale_flash", 10],
    ],
    [
        "woocommerce_sidebar" => ["woocommerce_get_sidebar", 10],
    ]
];


/**
 * Add theme support
 */
function bs_add_woocommerce_support()
{
    add_theme_support('woocommerce', [
        'thumbnail_image_width' => 300,
        'thumbnail_image_height' => 468
    ]);
}

add_action('after_setup_theme', 'bs_add_woocommerce_support');


/**
 * Inject css into head
 * Proper way to enqueue scripts and styles
 */
function add_custom_script()
{
    wp_enqueue_style('font-style', get_template_directory_uri() . '/woocommerce/dist/front.css?v=' . time());
    wp_enqueue_script('front-script', get_template_directory_uri() . '/woocommerce/dist/script/front-min.js', array(), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'add_custom_script');


function kampang()
{
    $attribute_taxonomies = wc_get_attribute_taxonomies();
//    if (!empty($attribute_taxonomies)) {
//        foreach ($attribute_taxonomies as $attr) {
//            var_dump($attr);
//        }
//    }
}

add_action('woocommerce_single_product_summary', 'kampang', 10);


add_filter('woocommerce_get_image_size_gallery_thumbnail', function ($size) {
    return array(
        'width' => 500,
        'crop' => 1,
    );
});

foreach ($removeHook as $name) {
    $key = array_keys($name)[0];
    remove_action($key, $name[$key][0], $name[$key][1]);
}