<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 11/5/19
 * Time: 1:19 AM
 */

defined('ABSPATH') || exit;
@include "add-image-to-product-attribute.php";

// Add related product
add_action('woocommerce_custom__related-product', 'woocommerce_output_related_products', 10);
add_action('woocommerce_custom__product-title', 'woocommerce_template_single_title', 10);
add_action('woocommerce_single_product_summary', 'size_guide_button', 60);
add_action('woocommerce_before_shop_loop_item_title', 'hover_thumb_loop', 60);


// Add image thumbnail into product attributes
//add_action('category_add_form_fields', "add_image_to_product_attribute", 10, 2);

if (!function_exists('size_guide_button')) {

    function size_guide_button()
    {
        wc_get_template('single-product/size-guide-button.php');
    }

}


if (!function_exists("hover_thumb_loop")) {
    function hover_thumb_loop()
    {
        global $product;
        $galleryImages = $product->gallery_image_ids;
        $primaryViewFeatured = $galleryImages[0];
        $featured = wp_get_attachment_image($primaryViewFeatured, array(500, 600));

        if ($featured) {
            echo "<div class='hover-thumbnail'>$featured</div>";
        }
    }
}