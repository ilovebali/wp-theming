<?php

$pageDetail = getPageData()

?>
<section id="single_accomodation" class="single_detail">
    <div class="content_section single-page-gallery">
        <h1 class="content-main-title">
            <?php the_title() ?>
        </h1>
        <div class="gallery_thumbs">

            <?php if ($pageDetail->gallery) : foreach ($pageDetail->gallery as $key => $gallery) : ?>
            <?php
            $image = $gallery['image'];
            $backgroundImage = $gallery['image'];

            if ($gallery['thumb']) {
                $image = $gallery['thumb'];
            }

            ?>

            <div class="gallery-thumbs-container">
                <div class="gallery-item bg-cover" style="background-image: url(<?php echo $backgroundImage ?>)">

                <div class="thumb">
                    <img src="<?php echo $image ?>" style="opacity: 0">
                </div>
            </div>

        </div>
        <?php endforeach;
        endif; ?>

    </div>
    </div>
</section>