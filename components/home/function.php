<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/23/18
 * Time: 9:53 PM
 */

function getFeaturedGallery()
{
    $galleries = getPostTypeListing('galleries');
    $galleryListing = [];

    if ($galleries) {
        foreach ($galleries as $key => $gallery) {
            $meta = get_post_meta($gallery->ID, 'galleryOptions', true);
            if ($meta && $meta['home_featured'] === 'on') {
                if ($gallery && $gallery->gallery) {
                    foreach ($gallery->gallery as $item) {
                        array_push($galleryListing, $item);
                    }
                }

            }
        }
    }
    return $galleryListing;
}