<?php
@include "function.php";
global $accommodations, $language;
$gallery = getFeaturedGallery();
$pageDetail = getPageData(true);
$facilities = getPostTypeListing('facilities');
$servicePageDetail = getChildDetailByPageId($pageDetail, 'service');
?>
<?php heroBanner($pageDetail->featured_image) ?>

<?php if ($pageDetail) : ?>
    <section class="home_content">
        <div class="main-banner slider__container"
             data-options='{"autoPlay" : true}'
             data-parallax=".parallax-container-about">
            <?php if ($pageDetail && $pageDetail->postMeta['gallery']) : foreach ($pageDetail->postMeta['gallery'] as $item) : ?>
                <div class="item">
                    <div class="background-container parallax-container-about">
                        <img src="<?php echo $item['large'] ?>"/>
                    </div>
                </div>
            <?php endforeach; endif; ?>
        </div>
        <div class="content-container">
            <article class="content__parallax" data-reverse="true" data-speed="30">
                <h2 class="__font-heading"><?php echo $pageDetail->post_title ?></h2>
                <?php echo apply_filters("the_content", $pageDetail->post_content) ?>
            </article>
        </div>
    </section>
<?php endif; ?>


<section class="accommodation-listing">
    <?php fullSliderComponent(getLanguage("accommodation"), $accommodations, function ($post) {
        $price = $post->postMeta['price'];
        if ($price) {
            echo "<div class='room-price'>$price[regular_price]</div>";
        }
        echo "<div class='button discover-more'><a href='$post->slug'>".getLanguage("discover")."</a> </div>";
    }, function ($post) {
        echo "<div class='item dot'>$post->post_title</div>";
    }) ?>
</section>


<?php if ($facilities) : ?>
    <section class="sunsuko-facilities">
        <h2 class="__font-heading">
            <?php echo getLanguage("services") ?></h2>
        <div class="card-container">
            <?php foreach ($facilities as $key => $item) : ?>
                <div class="card">
                    <div class="featured-card">
                        <div class="featured banner__parallax"
                             data-reverse="true"
                             style="background-image: url(<?php echo $item->featured_image ?>)"></div>
                        <div class="detail">
                            <div class="main-title __font-heading">
                                <?php echo $item->post_title ?>
                            </div>
                            <div class="button button__facilities" data-item="facility__<?php echo $key ?>">
                                <?php echo getLanguage("discover") ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
<?php endif; ?>


<?php if ($servicePageDetail) : ?>
    <section class="service-activities">
        <div class="gallery-container">
            <div class="main-title content__parallax">
                <h2 class="__font-heading">
                    <?php echo $servicePageDetail->post_title; ?>
                </h2>
            </div>
            <?php navigationSlider('activities-slider-navigation') ?>
            <div class="gallery-inner-container">
                <div class="slider__container" data-navigation="activities-slider-navigation"
                     data-parallax=".parallax-container">
                    <?php if ($servicePageDetail->gallery) : foreach ($servicePageDetail->gallery as $item): ?>
                        <div class="item">
                            <img class="parallax-container" src="<?php echo $item['large'] ?>">
                        </div>
                    <?php endforeach; endif; ?>
                </div>
            </div>
        </div>
        <div class="content-services">
            <div class="inner-container">
                <?php echo apply_filters("the_content", $servicePageDetail->post_content) ?>
            </div>

        </div>
    </section>
<?php endif; ?>
