<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/10/19
 * Time: 7:07 PM
 */

function accommodationListingComponent()
{
    global $accommodations;
    if ($accommodations) : ?>
        <section class="sunsuko-facilities">
            <div class="__font-heading">
                Our Accommodation
            </div>
            <div class="main-container">
                <div class="slider__container" data-type="split_slider:facilitiesDescription:facilitiesNavigation">
                    <?php foreach ($accommodations as $key => $facility) : ?>
                        <div class="activities">
                            <div class="featured">
                                <img src="<?php echo $facility->featured_image; ?>"
                                     alt="<?php echo $facility->post_title; ?>"
                                     class="background-image">
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>

                <div id="facilitiesDescription" class="activities-description">
                    <?php foreach ($accommodations as $key => $room) : ?>
                        <div class="description-content">
                            <h2 class="main-heading">
                                <?php echo $room->post_title; ?>
                            </h2>
                            <div class="description">
                                <?php echo $room->ecerpt ?>
                            </div>
                            <div class="button">
                                <a href="<?php echo $room->slug ?>">Discover more</a>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
            <div id="facilitiesNavigation" class="navigation-slider">
                <?php foreach ($accommodations as $key => $room) : ?>
                    <div class="page">
                        <?php echo $room->post_title; ?>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
    <?php endif;
}
