<?php
global $accommodations;
$postData = getPageData();
heroBanner($postData->postMeta['imageFeaturedSecondary']['full']);
?>

<section id="single_accomodation" class="single_detail">

    <div class="accommodation-single-content">
        <div class="welcome_content box-5o-one single-title">
            <h1 class="__font-heading"><?php the_title() ?></h1>
            <div class="room-description">
                <div class="inner-container">
                    <?php echo apply_filters('the_content', $postData->post_content) ?>
                </div>
            </div>
        </div>
        <div class="sidebar box-5o-one single-content-detail room-price-container">
            <div class="room-price">
                <?php echo $postData->postMeta['price']['regular_price'] ?>
            </div>
            <?php echo sprintf(getLanguage('price_detail'), $postData->postMeta['price']['extra_bed'], $postData->postMeta['price']['hi_season']) ?>
            <!--                    Incl. Breakfast / Extra Bed 30$<br>-->
            <!--                    Kitas Holder Special Price-->
        </div>
    </div>


    <section class="accommodation-gallery-listing">
        <?php navigationSlider('navigationPrevNext') ?>
        <div class="slider__container" data-options='{"pageDots": true, "autoPlay": true}'
             data-parallax=".parallax-image"
             data-navigation="navigationPrevNext">
            <?php if ($postData->gallery) : foreach ($postData->gallery as $key => $image) : ?>
                <div class="galleries">
                    <div class="image ">
                        <?php
                        $singleImage = $image['image'];

                        if ($image['large']) {
                            $singleImage = $image['large'];
                        }
                        ?>
                        <img class="parallax-image" src="<?php echo $singleImage ?>"/>
                    </div>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </section>

    <?php CardSliderWithTitle(getLanguage("room"), $accommodations, true, true) ?>
</section>