<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/11/19
 * Time: 1:29 AM
 * @param $id string
 */
function navigationSlider($id)
{
    ?>
    <div id="<?php echo $id ?>" class="navigation-prev-next-default">
        <div class="prev-next-navigation prev ti-arrow-left __prev"></div>
        <div class="prev-next-navigation prev ti-arrow-right __next"></div>
    </div>
    <?php
}