<?php
$postData = getPageData();
$facilities = getPostTypeListing('facilities');
?>
<section class="banner_slider full-height">
    <div class="">
        <div class="hero_image full-height bg-cover bg-top-pos"
             style="background-image:url(<?php echo $postData->featured_image ?>)"></div>
    </div>
    <div class="scroll-down">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86.001 86.001">
            <path d="M5.9 21c-1.35-1.33-3.54-1.33-4.9 0-1.34 1.34-1.34 3.5 0 4.85L40.56 65c1.36 1.34 3.54 1.34 4.9 0L85 25.85c1.34-1.34 1.34-3.5 0-4.85-1.37-1.33-3.56-1.33-4.9 0L43 56.7 5.9 21z"
                  fill="#030104"></path>
        </svg>
    </div>
</section>
<section class="container-row-one facilities">

    <?php if ($facilities) : foreach ($facilities as $key => $facility) : ?>
        <div class="container-row-one facility">
            <div class="banner thumb-left bg-cover" style="background-image: url()">
                <img src="<?php echo $facility->featured_image ?>">
            </div>
            <div class="padding-medium facilities-detail">
                <h2 class="sub-heading"><?php echo $facility->post_title ?></h2>
            </div>
        </div>

    <?php endforeach; endif; ?>

</section>

