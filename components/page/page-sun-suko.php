<?php
$pageData = getPageData();
?>
<div class="page-story-container">
    <?php heroBanner($pageData->featured_image) ?>

    <section class="single_detail page-about">
        <div class="overlay content__parallax" data-speed="80"></div>
        <div class="flex inner-container">
            <div class="photo-profile box-5o-one content__parallax"
                 data-disabled-mobile="true"
                 data-speed="90">
                <div class="self-portrait">
                    <?php foreach ($pageData->postMeta['gallery'] as $gallery) : ?>
                        <img src="<?php echo $gallery['full'] ?>">
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="sidebar box-5o-one single-content-detail content__parallax"
                 data-disabled-mobile="true"
                 data-speed="50">
                <article class="single-content-article">
                    <h1 class="__font-heading"><?php echo $pageData->post_title ?></h1>
                    <div class="content-profile">
                        <?php echo apply_filters("the_content", $pageData->post_content) ?>
                    </div>
                </article>
            </div>
        </div>

    </section>
</div>
