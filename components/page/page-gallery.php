<div class="page-gallery">
    <?php
    $galleries = getPostTypeListing('galleries');
    $galleryItems = [];
    if ($galleries) : foreach ($galleries as $gallery) :
        if ($gallery && $gallery->postMeta['gallery']) {
            foreach ($gallery->gallery as $item) {
                array_push($galleryItems, $item);
            }
        }
        ?>
    <?php
    endforeach;endif;
    ?>

    <div class="full-slider-section">
        <?php navigationSlider('page-gallery-slider') ?>
        <div class="full-slider slider__container"
             data-parallax=".background-featured"
             data-disabled-at="600"
             data-navigation="page-gallery-slider">
            <?php
            foreach ($galleryItems as $key => $post) :
                $image = $post['full'];
                if (!$image) $image = $post['image']
                ?>
                <div class="slide-container">
                    <div class="slide background-featured"
                         style="background-image: url(<?php echo $image ?>)">
                    </div>
                </div>
            <?php
            endforeach;
            ?>
        </div>
    </div>
</div>