<?php
global $getOption;
$pageDetail = getPageData();
$mapLocation = $getOption["google_map_option"];
?>
<section class="box-10o-one full-height contact-container">
    <?php heroBanner($pageDetail->featured_image, $pageDetail->post_title) ?>
    <div class="map-location content-item">
        <div class="google-map">
            <div id="map-location"></div>
        </div>
        <div class="contact-form content-item">
            <div class="display-tb">
                <div class="display-cell">
                    <h2 class="__font-heading">
                        <?php echo $pageDetail->post_title ?>
                    </h2>
                    <div class="sort-content">
                        <?php echo apply_filters('the_content', $pageDetail->post_content) ?>

                    </div>
                    <div id="show-form" class="button-square-border --block --dark button-show-form">
                        Send Inquiry
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>

    window.mapLatLang = {
        lat: <?php echo $mapLocation["loc"]["lt"] ?>,
        lng: <?php echo $mapLocation["loc"]["ln"] ?>,
        markerIcon: '<?php echo get_template_directory_uri() ?>/assets/images/marker.png'
    };

</script>