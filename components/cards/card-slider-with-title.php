<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/12/19
 * Time: 12:07 PM
 * @param $title
 * @param $postListing
 * @param bool $goToSlug
 * @param bool $removeSameItem
 */

function CardSliderWithTitle($title, $postListing, $goToSlug = false, $removeSameItem = false)
{
    $listing = null;
    if (is_array($postListing)) {
        $listing = $postListing;
    } else {
        $listing = getPostTypeListing($postListing);
    }

    if ($removeSameItem) {
        $listing = array_filter($listing, function ($item) {
            global $post;
            return $post->ID != $item->ID;
        });
    }

    $id = uniqid('cardSlider_');

    if ($listing) : ?>
        <section class="card-listing home">
            <div class="header">
                <div class="__font-heading">
                    <?php echo $title; ?>
                </div>
            </div>

            <?php navigationSlider($id) ?>

            <div class="slider__container" data-navigation="<?php echo $id ?>">
                <?php foreach ($listing as $key => $item) : ?>
                    <div class="listing">
                        <?php if ($item->post_content && !$goToSlug): ?>
                            <div class="description">
                                <div class="wrapper" id="facilities-<?php echo $key ?>"
                                     data-card-type="cardWithOverlayDescription">
                                    <div class="description-container">
                                        <?php echo apply_filters('the_content', $item->post_content); ?>
                                    </div>
                                    <div class="button __close-button ti-close"></div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="featured" style="background-image: url(<?php echo $item->featured_image ?>)">
                            <?php if ($item->gallery && !$goToSlug) : ?>
                                <div class="gallery-slider">
                                    <div class="slider__container"
                                         data-options='{"pageDots": true, "draggable": false}'>
                                        <?php foreach ($item->gallery as $i => $slide) : ?>
                                            <img src="<?php echo $slide['large'] ?>">
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="content">
                                <h2 class="title">
                                    <?php echo $item->post_title ?>
                                </h2>

                                <?php if ($item->post_content && !$goToSlug): ?>
                                    <div class="read-more button __button-read-more"
                                         id="button-facilities-<?php echo $key ?>">
                                        <?php echo getLanguage("more") ?>
                                    </div>
                                <?php else : ?>
                                    <div class="read-more button">
                                        <a href="<?php echo $item->slug ?>"><?php echo getLanguage("more") ?></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
    <?php endif;
}