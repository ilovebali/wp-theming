<?php
/**
 * Created by PhpStorm.
 * User: machinegun45
 * Date: 10/10/19
 * Time: 7:19 PM
 * @param $title
 * @param $postType
 * @param $options
 * @param $navigation
 */
function fullSliderComponent($title, $postType, $options, $navigation)
{

    global $post;
    $posts = null;

    if (is_array($postType)) {
        $posts = $postType;
    } else {
        if ($postType) {
            $posts = getPostTypeListing($postType);
        } else {
            $posts = getPageData();
        }
    }

    $id = uniqid('fullSlider_');

    if ($posts) :?>

        <div class="full-slider-section">
            <?php navigationSlider($id) ?>
            <?php if ($title): ?><h2
                    class="__font-heading"><?php echo apply_filters('the_content', $title); ?></h2><?php endif; ?>
            <div class="full-slider slider__container"
                 data-parallax=".background-featured"
                 data-dot-page="dot-<?php echo $id; ?>"
                 data-navigation="<?php echo $id; ?>">
                <?php
                foreach ($posts as $key => $post) :
                    ?>
                    <div class="slide-container">
                        <div class="slide background-featured"
                             style="background-image: url(<?php echo $post->featured_image ?>)">
                            <div class="content">
                                <div class="content__parallax inner-container" data-speed="15">
                                    <?php
                                    echo "<div class='__font-heading slider-title'>$post->post_title</div>";
                                    if ($options) : $options($post);
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                endforeach;
                ?>
            </div>

            <?php if ($navigation) : ?>
                <div id="dot-<?php echo $id ?>" class="navigation-full-slider">
                    <?php foreach ($posts as $nav) {
                        echo $navigation($nav);
                    } ?>
                </div>
            <?php endif; ?>
        </div>

    <?php endif;
}